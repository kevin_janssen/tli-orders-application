package com.tli.orders.controller;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import com.tli.orders.model.ChangeQuantityRequest;
import com.tli.orders.model.Order;
import com.tli.orders.model.OrderLineItem;
import com.tli.orders.model.StatusMap;

public class OrderControllerHelper {

	Validator validator;

	StatusMap statusMap = new StatusMap();

	public OrderControllerHelper() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	public String validateOrder(Order order) {
		StringBuilder errorBuilder = new StringBuilder();
		for (ConstraintViolation<Order> violation : validator.validate(order)) {
			errorBuilder.append(violation.getMessage());
		}
		for (OrderLineItem orderLineItem : order.getLineItems()) {
			errorBuilder.append(validateOrderLineItem(orderLineItem));
		}
		return errorBuilder.toString();
	}

	public String validateOrderLineItem(OrderLineItem orderLineItem) {
		StringBuilder errorBuilder = new StringBuilder();
		for (ConstraintViolation<OrderLineItem> violation : validator.validate(orderLineItem)) {
			errorBuilder.append(violation.getMessage());
		}
		return errorBuilder.toString();
	}

	public String validateChangeQuantityRequest(ChangeQuantityRequest changeQuantityRequest) {
		StringBuilder errorBuilder = new StringBuilder();
		for (ConstraintViolation<ChangeQuantityRequest> violation : validator.validate(changeQuantityRequest)) {
			errorBuilder.append(violation.getMessage());
		}
		return errorBuilder.toString();
	}
}
