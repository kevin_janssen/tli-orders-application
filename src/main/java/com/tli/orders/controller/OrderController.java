package com.tli.orders.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tli.orders.exception.ResourceNotFoundException;
import com.tli.orders.exception.ValidationException;
import com.tli.orders.model.ChangeQuantityRequest;
import com.tli.orders.model.Order;
import com.tli.orders.model.OrderLineItem;
import com.tli.orders.model.OrderLineItemPK;
import com.tli.orders.model.StatusMap;
import com.tli.orders.repository.OrderLineItemRepository;
import com.tli.orders.repository.OrderRepository;

@RestController
@RequestMapping("/api/order")
public class OrderController {

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	OrderLineItemRepository orderLineItemRepository;

	OrderControllerHelper helper = new OrderControllerHelper();

	StatusMap statusMap = new StatusMap();

	@GetMapping("/{id}")
	public Order viewOrder(@PathVariable(name = "id") Long id) {
		Order order = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order", "id", id));
		order.setLineItems(orderLineItemRepository.findByOrderId(order.getId()));
		for (OrderLineItem item : order.getLineItems()) {
			item.setNumber(item.getOrderLineItemPK().getNumber());
			item.setOrderId(item.getOrderLineItemPK().getId());
		}
		order.setStatus(statusMap.statusMap.get(order.getStatusId()));
		return order;
	}

	@PostMapping
	public Order placeOrder(@Valid @RequestBody Order order) {
		String errors = helper.validateOrder(order);
		System.out.println(errors);

		if (errors.length() > 0) {
			throw new ValidationException(errors);
		} else {
			Order placedOrder = orderRepository.save(order);
			order.setStatus(statusMap.statusMap.get(order.getStatusId()));
			int itemOrderNum = 1;
			for (OrderLineItem item : placedOrder.getLineItems()) {
				item.setOrderLineItemPK(new OrderLineItemPK(itemOrderNum, placedOrder.getId()));
				item.setOrderId(placedOrder.getId());
				item.setNumber(itemOrderNum++);
				orderLineItemRepository.save(item);
			}

			return placedOrder;
		}

	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<String> cancelOrder(@PathVariable(name = "id") Long id) {
		Order order = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order", "id", id));

		if (order.getStatusId() == 3 || order.getStatusId() == 4) {
			return new ResponseEntity<>(String.format("Unable to cancel order %d because it is %s", order.getId(),
					statusMap.statusMap.get(order.getStatusId())), HttpStatus.FORBIDDEN);
		}
		orderRepository.cancelOrderById(order.getId());

		return new ResponseEntity<>(String.format("Cancelled order %d", order.getId()), HttpStatus.OK);
	}

	@PutMapping("/{orderId}/item/{itemId}")
	@Transactional
	public ResponseEntity<String> changeQuantity(@PathVariable(name = "orderId") Long orderId,
			@PathVariable(name = "itemId") int itemId, @Valid @RequestBody ChangeQuantityRequest request) {
		String errors = helper.validateChangeQuantityRequest(request);

		if (errors.length() > 0) {
			throw new ValidationException(errors);
		} else {
			Order order = orderRepository.findById(orderId)
					.orElseThrow(() -> new ResourceNotFoundException("Order", "id", orderId));
			if (orderLineItemRepository.findByIds(orderId, itemId).size() == 0) {
				throw new ResourceNotFoundException("Line Item", "number", itemId);
			}
			orderLineItemRepository.updateQuantityByIds(orderId, itemId, request.getQuantity());
		}
		return new ResponseEntity<>(String.format("Changed quantity of order %d, line item %d", orderId, itemId),
				HttpStatus.OK);
	}

	@DeleteMapping("/{orderId}/item/{itemId}")
	@Transactional
	public ResponseEntity<String> removeLineItem(@PathVariable(name = "orderId") Long orderId,
			@PathVariable(name = "itemId") int itemId) {
		Order order = orderRepository.findById(orderId)
				.orElseThrow(() -> new ResourceNotFoundException("Order", "id", orderId));

		if (orderLineItemRepository.findByIds(orderId, itemId).size() == 0) {
			throw new ResourceNotFoundException("Line Item", "number", itemId);
		}

		orderLineItemRepository.deleteLineItemsByIds(orderId, itemId);

		return new ResponseEntity<>(String.format("Removed line item %d of order %d", itemId, orderId), HttpStatus.OK);
	}

}
