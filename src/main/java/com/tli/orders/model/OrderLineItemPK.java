package com.tli.orders.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class OrderLineItemPK implements Serializable {

	private static final long serialVersionUID = -546870801349034196L;

	private int number;

	private long orderId;

	public OrderLineItemPK() {

	}

	public OrderLineItemPK(int number, long orderId) {
		this.number = number;
		this.orderId = orderId;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public long getId() {
		return orderId;
	}

	public void setId(long id) {
		this.orderId = id;
	}

	@Override
	public String toString() {
		return "OrderLineItemPK [number=" + number + ", orderId=" + orderId + "]";
	}
}
