package com.tli.orders.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "order_line_items")
@JsonIgnoreProperties(value = { "createdDate", "createdBy", "modifiedDate", "modifiedBy", "number",
		"orderId" }, allowGetters = true)
public class OrderLineItem implements Serializable {

	private static final long serialVersionUID = -8295120533203542790L;

	@EmbeddedId
	@JsonIgnore
	private OrderLineItemPK orderLineItemPK;

	@JsonInclude
	@Transient
	private int number;

	@JsonInclude
	@Transient
	private long orderId;

	@NotBlank(message = "Item name must not be blank.")
	private String name;

	@PositiveOrZero(message = "Price cannot be negative.")
	private BigDecimal price;

	@Positive(message = "Must have a quantity of at least 1.")
	private BigDecimal quantity;

	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	@JsonIgnore
	private Date createdDate;

	@JsonIgnore
	private int createdBy = 1;

	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	@JsonIgnore
	private Date modifiedDate;

	@JsonIgnore
	private int modifiedBy = 1;

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public OrderLineItemPK getOrderLineItemPK() {
		return orderLineItemPK;
	}

	public void setOrderLineItemPK(OrderLineItemPK orderLineItemPK) {
		this.orderLineItemPK = orderLineItemPK;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public int getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(int modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Override
	public String toString() {
		String pkToString = "null";
		if (orderLineItemPK != null) {
			pkToString = orderLineItemPK.toString();
		}
		return "OrderLineItem [orderLineItemPK=" + pkToString + ", name=" + name + ", price=" + price + ", quantity="
				+ quantity + ", createdDate=" + createdDate + ", createdBy=" + createdBy + ", modifiedDate="
				+ modifiedDate + ", modifiedBy=" + modifiedBy + "]";
	}

}
