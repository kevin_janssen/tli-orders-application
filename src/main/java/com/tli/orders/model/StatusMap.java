package com.tli.orders.model;

import java.util.HashMap;
import java.util.Map;

public class StatusMap {
	public Map<Integer, String> statusMap = new HashMap<Integer, String>();
	public Map<String, Integer> statusReverseMap = new HashMap<String, Integer>();

	public StatusMap() {
		statusMap.put(1, "New");
		statusMap.put(2, "Processing");
		statusMap.put(3, "In Transit");
		statusMap.put(4, "Delivered");
		statusMap.put(5, "Canceled");

		statusReverseMap.put("New", 1);
		statusReverseMap.put("Processing", 2);
		statusReverseMap.put("In Transit", 3);
		statusReverseMap.put("Delivered", 4);
		statusReverseMap.put("Canceled", 5);
	}

}
