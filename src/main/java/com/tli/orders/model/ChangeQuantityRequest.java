package com.tli.orders.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.Positive;

public class ChangeQuantityRequest implements Serializable {

	private static final long serialVersionUID = -271509135157994260L;

	@Positive(message = "Must have a quantity of at least 1.")
	private BigDecimal quantity;

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

}
