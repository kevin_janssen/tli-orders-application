package com.tli.orders.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.tli.orders.model.OrderLineItem;
import com.tli.orders.model.OrderLineItemPK;

public interface OrderLineItemRepository extends JpaRepository<OrderLineItem, OrderLineItemPK> {

	@Query("SELECT item FROM OrderLineItem item WHERE item.orderLineItemPK.orderId = :orderId")
	List<OrderLineItem> findByOrderId(@Param("orderId") Long orderId);

	@Query("SELECT item FROM OrderLineItem item WHERE item.orderLineItemPK.orderId = :orderId AND item.orderLineItemPK.number = :itemId")
	List<OrderLineItem> findByIds(@Param("orderId") Long orderId, @Param("itemId") int itemId);

	@Modifying
	@Transactional
	@Query("UPDATE OrderLineItem item SET item.quantity = :quantity "
			+ "WHERE item.orderLineItemPK.orderId = :orderId AND item.orderLineItemPK.number = :itemId")
	void updateQuantityByIds(@Param("orderId") Long orderId, @Param("itemId") int itemId,
			@Param("quantity") BigDecimal quantity);

	@Modifying
	@Transactional
	@Query("DELETE OrderLineItem item WHERE item.orderLineItemPK.orderId = :orderId AND item.orderLineItemPK.number = :itemId")
	void deleteLineItemsByIds(@Param("orderId") Long orderId, @Param("itemId") int itemId);
}
