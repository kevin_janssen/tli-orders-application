package com.tli.orders.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tli.orders.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

	@Modifying
	@Query("UPDATE Order o SET o.statusId = 5 WHERE o.id = :orderId AND (o.statusId != 3 OR o.statusId != 4 )")
	void cancelOrderById(@Param("orderId") Long orderId);
}
