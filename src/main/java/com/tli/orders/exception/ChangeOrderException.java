package com.tli.orders.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ChangeOrderException extends RuntimeException {

	private static final long serialVersionUID = -8579308272802280160L;
	private Long orderId;
	private String reason;

	public ChangeOrderException(Long orderId, String reason) {
		super(String.format("Could not change order due to %s'", orderId, reason));
		this.orderId = orderId;
		this.reason = reason;
	}

	public Long getOrderId() {
		return orderId;
	}

	public String getReason() {
		return reason;
	}

}
