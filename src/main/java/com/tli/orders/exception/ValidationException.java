package com.tli.orders.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ValidationException extends RuntimeException {

	private static final long serialVersionUID = -3956710709436226993L;

	public ValidationException(String errors) {
		super(String.format("Validation errors found: " + errors));
	}
}
