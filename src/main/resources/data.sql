INSERT INTO order_statuses(`name`, created_date, created_by, modified_date, modified_by)
VALUES
('New', NOW(), 1, NOW(), 1),
('Processing', NOW(), 1, NOW(), 1),
('In Transit', NOW(), 1, NOW(), 1),
('Delivered', NOW(), 1, NOW(), 1),
('Canceled', NOW(), 1, NOW(), 1);

INSERT INTO orders(status_id, created_date, created_by, modified_date, modified_by)
VALUES
(3, NOW(), 1, NOW(), 1);

INSERT INTO order_line_items(`number`, order_id, `name`, price, quantity, created_date, created_by, modified_date, modified_by)
VALUES
(1, 1, 'In Transit Line Item', 5.00, 4.5, NOW(), 1, NOW(), 1);
